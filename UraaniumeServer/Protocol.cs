﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UraaniumeCommon;

namespace UraaniumeServer
{
    public abstract class AbstractProtocol
    {
        private TcpClient client;
        public TcpClient Client
        {
            get { return client; }
        }

        private AbstractTcpServer server;
        public AbstractTcpServer Server
        {
            get { return server; }
        }

        Thread protocolThread;

        StreamReader streamReader;
        StreamWriter streamWriter;

        bool exit;

        public AbstractProtocol(TcpClient client, AbstractTcpServer server)
        {
            this.client = client;
            this.server = server;

            NetworkStream stream = client.GetStream();
            streamReader = new StreamReader(stream);
            streamWriter = new StreamWriter(stream);
        }

        public void Start()
        {
            exit = false;
            protocolThread = new Thread(MainLoop);
            protocolThread.Start();
        }

        private void MainLoop()
        {
            try
            {
                do
                {
                    string message = FetchMessage();
                    RpcCall call = Serializer.Deserialize(message) as RpcCall;
                    ProcessMessage(call);
                } while (!exit);
            }
            catch (IOException)
            {
                Console.WriteLine("Client leave!");
            }
        }

        private string FetchMessage()
        {
            string message = streamReader.ReadLine();
            message = message.Replace("##", "\n");
            message = message.Replace("\\#", "#");
            return message;
        }

        protected void SendResponse(RpcResult result)
        {
            string response = Serializer.Serialize(result);
            response = response.Replace("#", "\\#");
            response = response.Replace("\n", "##");
            streamWriter.WriteLine(response);
            streamWriter.Flush();
        }

        protected void Exit()
        {
            exit = true;
            server.DestroyProtocol(this);
        }

        protected void SendResponseAndExit(RpcResult response)
        {
            SendResponse(response);
            Exit();
        }

        protected abstract void ProcessMessage(RpcCall call);

        protected abstract void ClientLeave();

        public void Stop()
        {
            Client.Close();
            Exit();
        }
    }
}
