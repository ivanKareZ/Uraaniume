﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace UraaniumeServer
{
    public abstract class AbstractTcpServer
    {
        private IPAddress ipAddress;
        public IPAddress IpAddress
        {
            get { return ipAddress; }
        }

        private int port;
        public int Port
        {
            get { return port; }
        }

        Thread serverThread;
        List<AbstractProtocol> protocolPool;
        TcpListener server;
        bool exit;
        bool waitForClient;

        public AbstractTcpServer(string ipAddress, int port)
        {
            this.ipAddress = IPAddress.Parse(ipAddress);
            this.port = port;
            this.protocolPool = new List<AbstractProtocol>();
            server = new TcpListener(this.ipAddress, port);
            waitForClient = true;
        }

        public void Run()
        {
            exit = false;
            serverThread = new Thread(MainLoop);
            serverThread.Start();
        }

        void MainLoop()
        {
            server.Start();
            do
            {
                if (waitForClient)
                {
                    server.BeginAcceptTcpClient(new AsyncCallback(DoAcceptTcpClientCallback), server);
                    waitForClient = false;
                }
                Thread.Sleep(100);
            } while (!exit);
            server.Stop();
        }

        void DoAcceptTcpClientCallback(IAsyncResult ar)
        {
            TcpListener state = (TcpListener)ar.AsyncState;
            TcpClient client = state.EndAcceptTcpClient(ar);
            AbstractProtocol protocol = GetProtocol(client);
            lock (protocolPool)
            {
                protocolPool.Add(protocol);
            }
            protocol.Start();
            waitForClient = true;
        }

        public void Stop()
        {
            exit = true;
            lock (protocolPool)
            {
                if (protocolPool.Count > 0)
                {
                    for (int i = 0; i < protocolPool.Count; i++)
                    {
                        protocolPool[i].Stop();
                    }
                }
            }
        }

        public void DestroyProtocol(AbstractProtocol protocol)
        {
            lock (protocolPool)
            {
                protocolPool.Remove(protocol);
            }
        }

        protected abstract AbstractProtocol GetProtocol(TcpClient client);
    }
}
