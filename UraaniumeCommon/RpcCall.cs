﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UraaniumeCommon
{
    [Serializable]
    public class RpcCall
    {
        private string methodName;
        public string MethodName
        {
            get { return methodName; }
        }

        private RpcParams parameters;
        public RpcParams Parameters
        {
            get { return parameters; }
        }

        public RpcCall(string methodName, RpcParams parameters)
        {
            this.methodName = methodName;
            this.parameters = parameters;
        }


    }
}
