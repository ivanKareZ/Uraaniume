﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UraaniumeCommon
{
    [Serializable]
    public class RpcParams
    {
        protected Dictionary<string, string> parameters;

        public RpcParams()
        {
            parameters = new Dictionary<string, string>();
        }

        public int Count()
        {
            return parameters.Count;
        }

        public List<String> Names()
        {
            return new List<string>(parameters.Keys);
        }

        public void Add(string name, string value)
        {
            parameters.Add(name, value);
        }

        public string Get(string name)
        {
            return parameters[name];
        }

        public void Add(string name, object obj)
        {
            string serialized = Serializer.Serialize(obj);
            Add(name, serialized);
        }

        public T Get<T>(string name)
        {
            string obj = parameters[name];
            T result = (T)Serializer.Deserialize(obj);
            return result;
        }
    }
}
