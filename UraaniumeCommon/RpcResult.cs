﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UraaniumeCommon
{
    [Serializable]
    public class RpcResult
    {
        protected object result;

        public RpcResult(object result)
        {
            this.result = result;
        }

        public T GetResult<T>()
        {
            return (T)result;
        }
    }
}
