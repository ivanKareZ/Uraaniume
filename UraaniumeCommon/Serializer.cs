﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace UraaniumeCommon
{
    public class Serializer
    {
        public static string Serialize(object obj)
        {
            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, obj);
            stream.Seek(0, SeekOrigin.Begin);
            byte[] buffer = stream.ToArray();
            return Convert.ToBase64String(buffer);
        }

        public static object Deserialize(string obj)
        {
            Stream stream = new MemoryStream();
            byte[] bytes = Convert.FromBase64String(obj);
            stream.Write(bytes, 0, bytes.Length);
            stream.Seek(0, SeekOrigin.Begin);
            BinaryFormatter formatter = new BinaryFormatter();
            object result = formatter.Deserialize(stream);
            stream.Close();
            return result;
        }
    }
}
