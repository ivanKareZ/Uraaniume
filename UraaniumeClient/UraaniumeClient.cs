﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UraaniumeCommon;

namespace UraaniumeClient
{
    public class UraaniumeTcpClient
    {
        private IPAddress address;
        public IPAddress Address
        {
            get { return address; }
        }

        private int port;
        public int Port
        {
            get { return port; }
        }

        TcpClient client;

        StreamWriter streamWriter;
        StreamReader streamReader;

        public UraaniumeTcpClient(IPAddress address, int port)
        {
            this.address = address;
            this.port = port;
            client = new TcpClient();
        }

        public void Connect()
        {
            client.Connect(address.ToString(), port);
            Stream s = client.GetStream();
            streamWriter = new StreamWriter(s);
            streamReader = new StreamReader(s);
        }

        public void Disconnect()
        {
            client.Close();
        }

        public RpcResult CallRpcFunction(RpcCall call)
        {
            string serializedCall = Serializer.Serialize(call);
            SendMessage(serializedCall);
            string serializedResult = FetchResponse();
            RpcResult result = Serializer.Deserialize(serializedResult) as RpcResult;
            return result;
        }

        protected void SendMessage(string message)
        {
            message = message.Replace("#", "\\#");
            message = message.Replace("\n", "##");
            streamWriter.WriteLine(message);
            streamWriter.Flush();
        }

        protected string FetchResponse()
        {
            string response = streamReader.ReadLine();
            response = response.Replace("##", "\n");
            response = response.Replace("\\#", "#");
            return response;
        }
    }
}
