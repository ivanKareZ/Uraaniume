﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UraaniumeServer;
using System.Net.Sockets;
using UraaniumeCommon;
using UraaniumeClient;
using System.Net;
using System.Threading;
using System.Diagnostics;

namespace UnitTestProject1
{
    [TestClass]
    public class CommunicationTests
    {
        static AbstractTcpServer testServer;

        [ClassInitialize]
        public static void Init(TestContext contex)
        {
            testServer = new TestServer("127.0.0.1", 7897, contex);
            testServer.Run();
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            testServer.Stop();
        }

        [TestMethod]
        [Timeout(2000)]
        public void SendString()
        {
            RpcParams p = new RpcParams();
            p.Add("a", "Hello");
            RpcCall c = new RpcCall("STRTST", p);

            UraaniumeTcpClient client = new UraaniumeTcpClient(IPAddress.Parse("127.0.0.1"), 7897);
            client.Connect();
            RpcResult result = client.CallRpcFunction(c);
            string resultString = result.GetResult<string>();
            client.Disconnect();
            Assert.AreEqual("OK", resultString);
        }
    }

    class TestServer : AbstractTcpServer
    {
        TestContext context;

        public TestServer(string ipAddress, int port, TestContext context) : base(ipAddress, port)
        {
            this.context = context;
        }

        protected override AbstractProtocol GetProtocol(TcpClient client)
        {
            return new TestProtocol(client, this, context);
        }
    }

    class TestProtocol : AbstractProtocol
    {
        TestContext context;

        public TestProtocol(TcpClient client, AbstractTcpServer server, TestContext context) : base(client, server)
        {
            this.context = context;
        }

        protected override void ClientLeave()
        {
            context.WriteLine("Client leave!");
        }

        protected override void ProcessMessage(RpcCall call)
        {
            switch (call.MethodName)
            {
                case "STRTST":
                    StringTest(call.Parameters);
                    break;
                default:
                    break;
            }
        }

        void StringTest(RpcParams parameters)
        {
            string stringParameter = parameters.Get("a");
            RpcResult result;
            if (stringParameter == "Hello")
                result = new RpcResult("OK");
            else
                result = new RpcResult("ERROR");
            SendResponseAndExit(result);
        }
    }
}
