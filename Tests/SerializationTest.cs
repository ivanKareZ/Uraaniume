﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UraaniumeCommon;

namespace Tests
{
    [TestClass]
    public class SerializationTest
    {
        [TestMethod]
        public void SerializeObjectTest()
        {
            string
                pName = "Name",
                pValue = "Value",
                mName = "MyMethod";
            RpcParams p = new RpcParams();
            p.Add(pName, pValue);
            RpcCall c = new RpcCall(mName, p);
            string serialized = Serializer.Serialize(c);
            RpcCall deserialized = Serializer.Deserialize(serialized) as RpcCall;
            Assert.AreEqual(pValue, deserialized.Parameters.Get(pName));
            Assert.AreEqual(mName, deserialized.MethodName);
        }

        [TestMethod]
        public void SerializeIntegerTest()
        {
            int original = 5;
            string serialized = Serializer.Serialize(original);
            int deserialized = (int)Serializer.Deserialize(serialized);
            Assert.AreEqual(original, deserialized);
        }

        [TestMethod]
        public void SerializeFloatTest()
        {
            float original = 5.12f;
            string serialized = Serializer.Serialize(original);
            float deserialized = (float)Serializer.Deserialize(serialized);
            Assert.AreEqual(original, deserialized);
        }

        [TestMethod]
        public void SerializeDoubleTest()
        {
            double original = 5.12;
            string serialized = Serializer.Serialize(original);
            double deserialized = (double)Serializer.Deserialize(serialized);
            Assert.AreEqual(original, deserialized);
        }

        [TestMethod]
        public void SerializeBoolTest()
        {
            bool original = true;
            string serialized = Serializer.Serialize(original);
            bool deserialized = (bool)Serializer.Deserialize(serialized);
            Assert.AreEqual(original, deserialized);

            original = false;
            serialized = Serializer.Serialize(original);
            deserialized = (bool)Serializer.Deserialize(serialized);
            Assert.AreEqual(original, deserialized);
        }

        [TestMethod]
        public void SerializeByteTest()
        {
            byte original = 128;
            string serialized = Serializer.Serialize(original);
            byte deserialized = (byte)Serializer.Deserialize(serialized);
            Assert.AreEqual(original, deserialized);
        }

        [TestMethod]
        public void SerializeLongTest()
        {
            long original = long.MaxValue;
            string serialized = Serializer.Serialize(original);
            long deserialized = (long)Serializer.Deserialize(serialized);
            Assert.AreEqual(original, deserialized);
        }

    }
}
